@extends('layouts.master')

@section('content')
<section class="content-header">
      <h1>
        REKAPITULASI REAL COUNT PEMILU 2019
        </br>PROVINSI JAWABARAT
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
      </br>
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
  <div class="info-box bg-white">
  <span class="info-box-icon"><img class="img-logo bg-white" src="{{asset('images/kotak.png')}}"/></span>
      <div class="info-box-content">    
        <span class="info-box-number">{{$Data['tps']}}</span>  
        <span class="progress-description">
          Jumlah TPS
        </span>
      </div>
  <!-- /.info-box-content -->
  </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
      <div class="info-box bg-white">
        <span class="info-box-icon"><img class="img-logo bg-white" src="{{asset('images/forma1.png')}}"/></span>
      <div class="info-box-content">    
        <span class="info-box-number">{{$Data['pemilih']}}</span>  
        <span class="progress-description">
          Jumlah DPT
        </span>
      </div>
  <!-- /.info-box-content -->
  </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
        <div class="info-box bg-white">
          <span class="info-box-icon"><img class="img-logo bg-white" src="{{asset('images/forma2.png')}}"/></span>
      <div class="info-box-content">    
          <span class="info-box-number">{{$Data['suaraMasuk']}}</span>  
          <span class="progress-description">
            Jumlah Suara Masuk
          </span>         
      </div>
  <!-- /.info-box-content -->
  </div>
  </div>
    </section>
    <section class="content container-fluid" id="parallax1">

<div class="box box-warning">
        <div class="box-header with-border">
        <i class="fa fa-pie-chart"></i>

          <h3 class="box-title">Persentase Perolehan Suara</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div id="chartPie" style="height: 230px;"></div>
        </div>
        <!-- /.box-body-->
      </div>
      <!-- /.box -->
    </div>

</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/echarts/4.1.0/echarts.min.js"></script>
<script>
$(function(){
  'use strict'
 
  /**************** PIE CHART ************/
  var pieData = [{
    name: 'Prosentase Perolehan Suara',
    type: 'pie',
    radius: '80%',
    center: ['50%', '57.5%'],
    data: <?php echo json_encode($Data['data_suara']); ?>,
    label: {
      normal: {
        fontFamily: 'Roboto, sans-serif',
        fontSize: 11
      }
    },
    labelLine: {
      normal: {
        show: false
      }
    },
    markLine: {
      lineStyle: {
        normal: {
          width: 1
        }
      }
    }
  }];
  var pieOption = {
    tooltip: {
      trigger: 'item',
      formatter: '{a} <br/>{b}: {c} ({d}%)',
      textStyle: {
        fontSize: 11,
        fontFamily: 'Roboto, sans-serif'
      }
    },
    legend: {},
    series: pieData
  };
  var pie = document.getElementById('chartPie');
  var pieChart = echarts.init(pie);
  pieChart.setOption(pieOption);
   /** making all charts responsive when resize **/
});
</script>
@endsection