  @extends('layouts.master')

  @section('content')
  <!-- Main content -->
<section class="content container-fluid" >
<div class="box box-danger">
            <div class="box-header">
              <h3 class="box-title">Data Provinsi</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            
              <table id="table" class="table table-bordered table-hover">            
                <thead>
                <tr>
                  <th class="col-sm-1">No.</th>
                  <td style="display:none;">
                  <th class="col-sm-2">Provinsi</th>
                  <th class="col-sm-2">Total Suara </th>
                  <th class="col-sm-2">Jokowi - Ma'ruf </th>
                  <th class="col-sm-2">Prabowo - Sandi</th>
                  <th class="col-sm-2">Suara Tidak Sah </th>
                  <th class="col-sm-2">Action</th>
                </tr>
                </thead>
                <tbody id="kecamatan">
                    @php
                    $no = 1;
                    @endphp

                    @foreach($provinsi as $items)
                    <tr>
                    <td>{{$no++}}</td>
                    
                        <td>{{$items->provinsi}}</td>
                        <td>{{$items->total_suara}}</td>
                        <td>{{$items->suara_paslon_1}}</td>
                        <td>{{$items->suara_paslon_2}}</td>
                        <td>{{$items->tidak_sah}}</td>
                        <td>
                            <form method="post">
                                {{ csrf_field() }}                               
                            <div class="btn-group btn-group-sm" style="float: none;">
                            <a href="{{url('/tps_db',$items->provinsi)}}" class=" btn btn-sm btn-primary">Kabupaten/Kota</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                </table> 
                </form> 
            </div>
        </div>        

</section>
<!-- /.content -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
  $(document).ready(function() {
    $('#table').DataTable({
    "bPaginate": true,
    "bLengthChange": false,
    "bFilter": true,
    "bInfo": true,
    "bAutoWidth": false,
    "aoColumnDefs": [ {
   "aTargets": [3],
   "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
     var $currencyCell = $(nTd);
     var commaValue = $currencyCell.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
     $currencyCell.text(commaValue);
   }
}]
    });
  });
  function formatNumber(n) {
    return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
</script>

<!-- <script type="text/javascript">
    $(document).ready(function(){
      $('#kab').on('change',function(e){
        var no = 0;
        var idkec=e.target.value;
        var idkab = e.target.value;
        $.get('{{url('kecamatan')}}/'+idkab,function(data){
          console.log(idkab);
          console.log(data);
          $('#kecamatan').empty();
          $.each(data,function(index, element){
            no++
            $('#kecamatan').append("<tr><td>"+no+"</td><td>"+element.nama_kec+"</td>"+"<td>"+element.jml_suara+"</td>"+"<td>"+"<form method='post'>"+"<a href='{{ url('kelurahan') }}/"+element.id+"' class='btn btn-sm btn-primary'>Edit</a>"+"</td></tr>");
          });
        });
      });
    });
</script> -->
@endsection