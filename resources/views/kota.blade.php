@extends('layouts.master')

@section('content')
<!-- Main content -->
<section class="content container-fluid" >
<div class="box box-danger">
          <div class="box-header with-border">
            <h3 class="box-title">Data Kota / Kabupaten</h3>
          </div>
          
          <!-- /.box-header -->
          <div class="box-body">
          <table id="table" class="table table-striped table-bordered">
              <thead>
              <tr>
              <th class="col-sm-1">No.</th>
                  <td style="display:none;">
                  <th class="col-sm-2">Kota</th>
                  <th class="col-sm-2">Total Suara </th>
                  <th class="col-sm-2">Jokowi - Ma'ruf </th>
                  <th class="col-sm-2">Prabowo - Sandi</th>
                  <th class="col-sm-2">Suara Tidak Sah </th>
                  <th class="col-sm-2">Action</th>
              </tr>
              </thead>
              <tbody id="kota">
                  @php
                  $no = 1;
                  @endphp

                  @foreach($kota as $items)
                  <tr>
                    <td>{{$no++}}</td>
                    
                        <td>{{$items->kota}}</td>
                        <td>{{$items->total_suara}}</td>
                        <td>{{$items->suara_paslon_1}}</td>
                        <td>{{$items->suara_paslon_2}}</td>
                        <td>{{$items->tidak_sah}}</td>
                        <td>
                            <form method="post">
                                {{ csrf_field() }}                               
                            <div class="btn-group btn-group-sm" style="float: none;">
                            <a href="{{url('/kota',$items->kota)}}" class=" btn btn-sm btn-primary">Kecamatan</a>
                        </td>
                    </tr>
                  @endforeach
              </tbody>
              </table>  
          </div>
      </div>
</section>
<!-- /.content -->
<script>
$(document).ready(function() {
  $('#table').DataTable();
} );
</script>

<!-- <script type="text/javascript">
  $(document).ready(function(){
    $('#kab').on('change',function(e){
      var no = 0;
      var idkab = e.target.value;
      $.get('{{url('kecamatan')}}/'+idkab,function(data){
        console.log(idkab);
        console.log(data);
        $('#kecamatan').empty();
        $.each(data,function(index, element){
          no++
          $('#kecamatan').append("<tr><td>"+no+"</td><td>"+element.nama_kec+"</td>"+"<td>"+element.jml_suara+"</td></tr>");
        });
      });
    });
  });
</script> -->
@endsection