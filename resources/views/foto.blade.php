@extends('layouts.master')

@section('content')
<!-- Main content -->

<div class="row">

    @foreach($data['foto'] as $items)
    <div class="col-sm-3">
          <!-- Box Comment -->
        <div class="box box-widget">
            <div class="box-header with-border">
              <div class="user-block">                
                <p>{{$items->id_tps}}</p>
                <p>Shared publicly - 7:30 PM Today</p>
              </div>
              <!-- /.user-block -->
              <div class="box-tools">                
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>                
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <a href="{{$items->foto}}" target="_blank">
              <img src="{{$items->foto}}" alt="Lights" style="width:100%; height:200px">
              </a>
              <p>{{$items->ket}}</p>
              <button type="button" class="btn btn-default btn-xs"><i class="fa fa-map-marker"></i> Location</button>              
              <span class="pull-right text-muted"><?=changeCoor($items->lat,$items->lng)?></span>
            </div>
        </div>
    </div>
    @endforeach
</div>    

<?php

function changeCoor($lat,$lng){
    $geocodeFromLatLong = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$lat.','.$lng.'&sensor=false&key=AIzaSyC83_nfkeUnT6aDSzAgtENHIOVvg20r6cs'); 
    $output = json_decode($geocodeFromLatLong);
    $status = $output->status;
    return ($status=="OK")?$output->results[1]->formatted_address:'Not Found';
}
?>
@endsection