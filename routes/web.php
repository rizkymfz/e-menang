<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();
//Route::get('/home', 'HomeController@pieChart');

Auth::routes();
Route::resource('kabupaten','kabupatenController');
Route::get('/kecamatan/{id_kabupaten}','kabupatenController@ajax');
Route::get('/tps_db/{provinsi}','kabupatenController@showKota');
Route::get('/kota/{kota}','kabupatenController@showKec');
Route::get('/kelurahan/{kec}','kabupatenController@showKel');
Route::get('/image','fotoController@showImage');
Route::resource('dashboard','dashboardController');