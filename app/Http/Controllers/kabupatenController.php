<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class kabupatenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $provinsi = DB::table('count_db')                    
                    ->select(DB::raw('tps_db.provinsi,sum(jumlah) as total_suara,sum(jmlPaslon1) as suara_paslon_1,sum(jmlPaslon2) as suara_paslon_2,sum(jmlFail) as tidak_sah'))
                    ->join('tps_db','tps_db.id_tps','=','count_db.id_tps')
                    ->groupBy('tps_db.provinsi')
                    ->get();
        // $kabupaten = kabupaten::all();
        return view('kabupaten', compact('provinsi'));
    }

    // public function ajax($idkab){
    //     if($idkab==0){
    //         $data = kecamatan::all();
    //     }else{
    //         $data = kecamatan::where('kecamatan.id_kabupaten', $idkab)
    //         ->get();
    //     }
    //     return $data;
    // }

    public function showKota($provinsi){
        $kota = DB::table('count_db')
        ->select(DB::raw('tps_db.kota,sum(jumlah) as total_suara,sum(jmlPaslon1) as suara_paslon_1,sum(jmlPaslon2) as suara_paslon_2,sum(jmlFail) as tidak_sah'))
        ->join('tps_db','tps_db.id_tps','=','count_db.id_tps')
        ->where('tps_db.provinsi', $provinsi)
        ->groupBy('tps_db.kota')
        ->get();
        return view ('kota', compact('kota'));
    }

    public function showKec($kota){
        $kecamatan = DB::table('count_db')
        ->select(DB::raw('tps_db.kec,sum(jumlah) as total_suara,sum(jmlPaslon1) as suara_paslon_1,sum(jmlPaslon2) as suara_paslon_2,sum(jmlFail) as tidak_sah'))
        ->join('tps_db','tps_db.id_tps','=','count_db.id_tps')
        ->where('tps_db.kota', $kota)
        ->groupBy('tps_db.kec')
        ->get();
        return view ('kecamatan', compact('kecamatan'));
    }

    public function showKel($kec){
        $kelurahan = DB::table('count_db')
        ->select(DB::raw('tps_db.kel,sum(jumlah) as total_suara,sum(jmlPaslon1) as suara_paslon_1,sum(jmlPaslon2) as suara_paslon_2,sum(jmlFail) as tidak_sah'))
        ->join('tps_db','tps_db.id_tps','=','count_db.id_tps')
        ->where('tps_db.kec', $kec)
        ->groupBy('tps_db.kel')
        ->get();
        return view ('kelurahan', compact('kelurahan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
