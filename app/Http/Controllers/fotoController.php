<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class fotoController extends Controller
{
    public function showImage(){
        $foto = DB::table('fotoTPS')        
        ->get();

        $data['foto']=$foto;
        return view ('foto', compact('data',$data));
    }
}
