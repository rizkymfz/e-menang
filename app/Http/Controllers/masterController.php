<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class masterController extends Controller
{
    public function pieChart(){
        $Data = array
        (
          "0" => array
                          (
                            "value" => 335,
                            "name" => "Apple",
                          ),
          "1" => array
                          (
                            "value" => 310,
                            "name" => "Orange",
                          )
                          ,
          "2" => array
                          (
                            "value" => 234,
                            "name" => "Grapes",
                          )
                          ,
          "3" => array
                          (
                            "value" => 135,
                            "name" => "Banana",
                          )
        );
return view('master',['Data' => $Data]);
    }
}
