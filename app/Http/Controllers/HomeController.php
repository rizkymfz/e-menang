<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

//     public function pieChart(){
//         $Data = array
//         (
//           "0" => array
//                           (
//                             "value" => 335,
//                             "name" => "Suara Belum Masuk",
//                           ),
//           "1" => array
//                           (
//                             "value" => 410,
//                             "name" => "Prabowo - Sandi",
//                           )
//                           ,
//           "2" => array
//                           (
//                             "value" => 234,
//                             "name" => "Jokowi - Ma'ruf",
//                           )
       
//         );
// return view('home',['Data' => $Data]);
//     }
}
