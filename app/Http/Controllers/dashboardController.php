<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\countTPS;

class dashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //jumlah tps
        $tps = DB::table('tps_db')->count();

        //jumlah pemilih
        $pemilih = DB::table('biodata_db')->count();

        //total suara
        $suaraMasuk = DB::table('count_db')
                      ->select(DB::raw('sum(jumlah) AS total'))->first();

        //Query show Count TPS
        $jmlSuara = DB::table('count_db')
                     ->select(DB::raw('SUM(jmlPaslon1) AS paslon1 ,SUM(jmlPaslon2) 
                     AS paslon2,SUM(jmlFail) AS tidak_sah'))
                     ->first();
        $DataSuara = array
        (
          "0" => array
                          (
                            "value" => $jmlSuara->paslon2,
                            "name" => "Prabowo - Sandi",
                          ),
          "1" => array
                          (
                            "value" => $jmlSuara->tidak_sah,
                            "name" => "Suara Tidak Sah",
                          )
                          ,
          "2" => array
                          (
                            "value" => $jmlSuara->paslon1,
                            "name" => "Jokowi - Ma'ruf",
                          )
       
        );
        $Data['tps']=$tps;
        $Data['pemilih']=$pemilih;
        $Data['suaraMasuk']=$suaraMasuk->total;
        $Data['data_suara']=$DataSuara;

        return view('dashboard',compact('Data',$Data));
    }

   


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
